import {defineConfig} from "vite";
import {ViteEjsPlugin} from "vite-plugin-ejs";
// import vike from 'vike/plugin'

export default defineConfig({
  plugins: [
    ViteEjsPlugin(),
    // vike()
  ]
});