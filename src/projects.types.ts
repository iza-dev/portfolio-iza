    type Project = {
    title: string;
    subTitle: string;
    project_information: Projectinformation;
    text: string;
    stacks: string[];
    images: Images;
  }
  
  type Images ={
    path_1?: Path;
    path_2?: Path;
    path_3?: Path;
    path_4?: Path;
    path_big_picture?: Path;
    path_miniature: Path;
  }
  
  type Path ={
    src: string;
    alt: string;
  }
  
  type Projectinformation ={
    category: string;
    client: string;
    project_date: string;
    project_url: string;
    project_gitlab: string;
  }

  export default Project