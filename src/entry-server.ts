import ejs from 'ejs';
import fs from 'fs/promises';
import Project from './projects.types'
export async function render() {
  const jsonFilePath = new URL('./projects.json', import.meta.url);
  const jsonContent = await fs.readFile(jsonFilePath, 'utf-8');
  const jsonData : Project[] = JSON.parse(jsonContent).projects;
  const templateData = {
    title: 'Portfolio • iza',
    description: 'This is my portfolio, which describes my skills as a frontend engineer and blockchain enthusiast.',
    projects: jsonData, 
  }

  const headerPartial = await fs.readFile('src/views/partials/header.ejs', 'utf-8')
  const footerPartial = await fs.readFile('src/views/partials/footer.ejs', 'utf-8')
  const infosContactPartial = await fs.readFile('src/views/partials/infos-contact.ejs', 'utf-8')
  const owlPartial = await fs.readFile('src/views/partials/owl.ejs', 'utf-8')
  const presentationPartial = await fs.readFile('src/views/sections/presentation.ejs', 'utf-8')
  const expertisePartial =  await fs.readFile('src/views/sections/expertise.ejs', 'utf-8')
  const experiencePartial =  await fs.readFile('src/views/sections/experience.ejs', 'utf-8')
  const contactPartial = await fs.readFile('src/views/sections/contact.ejs', 'utf-8')

  const headerHtml = ejs.render(headerPartial, templateData, {})
  const footerHtml = ejs.render(footerPartial, templateData, {})
  const infosContactHtml = ejs.render(infosContactPartial, templateData, {})
  const owlHtml = ejs.render(owlPartial, templateData, {})
  const presentationHtml = ejs.render(presentationPartial, templateData, {})
  const expertiseHtml = ejs.render(expertisePartial, templateData, {})
  const experienceHtml = ejs.render(experiencePartial, templateData, {})
  const contactHtml = ejs.render(contactPartial, templateData, {})

  const stackItems = templateData.projects[0].stacks.map((stack: string) => {
    return `<li>${stack}</li>`;
  }).join('');

  const head = `
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
  <meta name="description" content="${templateData.description}">
  <title>${templateData.title}</title>
  <link rel="icon" type="image/x-icon" href="/assets/favicon_ico/favicon-iza.ico" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
  `
  const html = `
  <div id="wrapper">
      <div id="menu-burger" class="burger">
      <span id="span-menu-burger"></span>
    </div>
    <div class="sidebar" id='wrapper-sidebar'>
      <div id='content-sidebar'>
          ${headerHtml}
          ${infosContactHtml}
      </div>
    </div>
    <div id="container">
      <div id="second-bg-on-top"></div>
      <div id="img-avatarInJungle">
          <picture>
              <source type="image/webp" media="(max-width: 1025px)" srcset="assets/images/900/avatar_jungle_Layer_2_900.webp">
              <source type="image/webp" media="(max-width: 2560px)" srcset="assets/images/2750/avatar_jungle_Layer_2_2750.webp">
              <img loading="lazy" id="layer_2" class='img-avatar' src='assets/images/2750/avatar_jungle_Layer_2_2750.png' alt="Avatar" title="Avatar au milieu d'un jardin">
          </picture>
          <picture>
              <source type="image/webp" media="(max-width: 1025px)" srcset="assets/images/900/avatar_jungle_Layer_1_900.webp">
              <source type="image/webp" media="(max-width: 2560px)" srcset="assets/images/2750/avatar_jungle_Layer_1_2750.webp">
              <img loading="lazy" id="layer_1" class='img-avatar' src='assets/images/2750/avatar_jungle_Layer_1_2750.png' alt="Avatar" title="Avatar au milieu d'un jardin">
          </picture>
              <h1 id="main-title">portfolio</h1>
          <picture>
              <source type="image/webp" media="(max-width: 1025px)" srcset="assets/images/900/avatar_jungle_Layer_0_900.webp">
              <source type="image/webp" media="(max-width: 2560px)" srcset="assets/images/2750/avatar_jungle_Layer_0_2750.webp">
              <img loading="lazy" id="layer_0" class='img-avatar' src='assets/images/2750/avatar_jungle_Layer_0_2750.png' alt="Avatar" title="Avatar au milieu d'un jardin">
          </picture>
          <div id="gradient-linear"></div>
      </div>
      <div id="bg-presentation"></div>
      <main id='wrapper-content'>
      ${presentationHtml}
      ${expertiseHtml}
      ${experienceHtml}
      ${contactHtml}
      </main>
   
      ${footerHtml}

    </div>
</div>
  `
  const html_project_details =`
  <div id="container-big-picture">
  <picture>
    <source type="image/webp" media="(max-width: 1025px)" srcset="${templateData.projects[0].images.path_big_picture}">
    <source type="image/webp" media="(max-width: 2560px)" srcset="${templateData.projects[0].images.path_big_picture}">
    <img id="big-picture-project" src="${templateData.projects[0].images.path_big_picture}" alt="${templateData.projects[0].images.path_big_picture}">
  </picture>
  <a name="link_header_project" href="#header-project">
    <div id="scroll-down"></div>
  </a>
  <header id="header-project">
    <div>
      <h1>${templateData.projects[0].title} </h1>
      <a name="link_experience" href="/#experience">
        <i id="icon-return" class="fas fa-reply"></i>
      </a>
      <ol>
        <li><a name="home_page" href="/">Home</a></li>
        <li><a name="link_experience" href="/#experience"> <span> &#128293 </span> Other projects <span> &#128293 </span></a></li>
      </ol>
    </div>
  </header>
</div>
<main id="content-project-details">
  <section id="infos-resume">
    <div id="sub-title-project">
      <h2>${templateData.projects[0].subTitle}</h2>
    </div>
    <div id="infos-project">
      <div id="container-infos-project">
        <div id='title-infos-project'>
          <h3>Project information</h3>
        </div>
        <ul>
          <li><strong>Category</strong>: ${templateData.projects[0].project_information.category}</li>
          <li><strong>Client</strong>: ${templateData.projects[0].project_information.client}</li>
          <li><strong>Project date</strong>: ${templateData.projects[0].project_information.project_date}</li>
          <li><strong>Project URL</strong>: <a name="url_recent_work" href="${templateData.projects[0].project_information.project_url}">${templateData.projects[0].project_information.project_url}</a></li>
          <li><strong>Repo</strong>: <a name="link_gitlab" href="${templateData.projects[0].project_information.project_gitlab}">Gitlab</a></li>
        </ul>
      </div>
    </div>
  </section>
  <section id="text-presentation-project">
    <p>
    ${templateData.projects[0].text}
    </p>
    <h4>Stack :</h4>
    <ul>
       ${stackItems}
    </ul>
  </section>
  <section id="slide-show-project"> 
  <carousel>
  <input type="radio" id="img1cb" checked name="images" />
  <input type="radio" id="img2cb" name="images" />
  <input type="radio" id="img3cb" name="images" />
  
  <div id="img1" class="wrapper-label">
      <label for="img1cb" title="first image">
          <picture>
              <source type="image/png" srcset="${templateData.projects[0].images.path_1}">
              <img loading="lazy" src="${templateData.projects[0].images.path_1}" alt="" >
          </picture>
      </label>
  </div>

  <div id="img2" class="wrapper-label">
      <label for="img2cb" title="second image">
          <picture>
              <source type="image/png" srcset="${templateData.projects[0].images.path_2}">
              <img loading="lazy" src="${templateData.projects[0].images.path_2}" alt="" >
          </picture>
      </label>
  </div>

  <div id="img3" class="wrapper-label">        
      <label for="img3cb" title="third image">
          <picture>
              <source type="image/png" srcset="${templateData.projects[0].images.path_3}">
              <img loading="lazy" src="${templateData.projects[0].images.path_3}" alt="" >
          </picture>
      </label>
  </div>
</carousel>

</section>
</main>

${footerHtml}


`
  return { html, head, html_project_details }
}
